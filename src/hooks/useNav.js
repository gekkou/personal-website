import { NavContext } from "context/NavContext";
import { useContext, useEffect, useRef } from "react"
import { useOnScreeen } from "./useOnScreen";

export const useNav = (navLinkId) => {
  const ref = useRef(null);
  const { setActiveLinkId } = useContext(NavContext);

  const isOnScreen = useOnScreeen(ref);

  useEffect(() => {
    if (isOnScreen) {
      setActiveLinkId(navLinkId)
    }
  }, [isOnScreen, setActiveLinkId, navLinkId])
  return ref;
}
