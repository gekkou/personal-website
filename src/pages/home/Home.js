import React from "react";
import './home.css'
import Navbar from 'components/navbar/Navbar';
import Header from "components/header/Header";
import Work from "components/work/Work";
import About from "components/about/About";
import Contact from "components/contact/Contact";
import Footer from "components/footer/Footer";
import NavProvider from 'context/NavContext';

export default function Home() {
  return (
    <>
      <NavProvider>
        <Navbar />
        <Header />
        <Work />
        <About />
        <Contact />
        <Footer />
      </NavProvider>
    </>

  )
}
