import React from "react";
import './blog.css'
import NavbarNew from 'components/navbarNew/NavbarNew';
import Footer from "components/footer/Footer";
import BlogPosts from "components/blogPosts/BlogPosts";

export default function Blog() {
  return (
    <div className="blog">
      <NavbarNew />
      <section className="blogWrapper">
        <h1 className="h3Titles h3TitlesWhite">
          Blog Posts
        </h1>
        <BlogPosts />
      </section>
      <Footer />
    </div>
  )
}
