import React from "react";
import { useNav } from 'hooks/useNav';
import './header.css'

export default function Header() {
  const headerRef = useNav("Header");

  return (
    <section id="homeSection"
      ref={headerRef}
      className="home side">
      <div className="hero">
        <h1 className="heroTitle">
          Hi, Im
          <br />
          Kevin Rizo
        </h1>
        <h2>
          <ul className="heroList">
            <li>
              Front End Developer
            </li>
            <li>
              Graphic Designer
            </li>
            <li>
              3D Artist
            </li>
          </ul>
        </h2>
      </div>
    </section>
  );
};
