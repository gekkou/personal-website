import React, { useState } from "react";
import { useNav } from 'hooks/useNav';
import './about.css';
import pp from 'media/images/pp.jpg';
import { IoIosArrowForward } from 'react-icons/io';
import { BsXCircleFill } from "react-icons/bs";
import { FaHeart, FaTelegramPlane } from "react-icons/fa";
import { GoCheck } from 'react-icons/go';

export default function About() {
  const aboutRef = useNav("About");
  const [leer, leerMas] = useState(false);

  const handleClickLeer = () => {
    leerMas(true);
  }
  return (
    <section
      ref={aboutRef}
      id="aboutSection"
      className="about side">
      <div className="readMoreFloating">
        {
          leer
          &&
          <div className="readMoreWindow">
            <BsXCircleFill onClick={() => leerMas(false)} />
            <div className="readMoreContent">
              <div className="readMoreContentLeft">
                <p className="h3TitlesWindow">
                  About Me
                </p>
                <img src={pp} className="pp" alt="Kevin Rizo Web Developer" />
              </div>
              <div className="readMoreContentRight">
                <p className="aboutParWindow">
                  Hi, my name is Kevin Rizo, I'm 27 years old, I'm from Matagalpa, Nicaragua.
                  <br /><br />
                  I Studied graphic desing and programming for a few months until now,
                  in fact I'm a self thaught person, since I was a child my interests were focus on arts besides technology, however,
                  my passion in this area increased once I discover a new and powerful OS as <b className="linux">GNU/Linux</b>, when I find out that I could increase the usability and resources from my desktop computer installing and customizing according to my needs, <span className="redBg">i felt in love  with <b className="linux">GNU/Linux </b><FaHeart className="love" /></span> and all it's possibilities and how the computer did what ever i ordered.
                  <br /><br />
                  I created this website to share with the world my skills and enthusiasm for technology, <s>and why not? to get a job in the field that I love so much, which is computer science.</s>
                  <br /><br />
                  Thanks you for let me share a little about myself to you.
                </p>
              </div>
            </div>
          </div>
        }
      </div>
      <p className="h3Titles">
        About Me
      </p>
      <div className="aboutTexts">
        <p className="aboutPar">
          My name is Kevin Rizo, I’m a Jack of all trades, and master of one,</p><ul className="aboutCheck aboutPar" > <li><GoCheck />FrontEnd Developer, </li>  <li><GoCheck />Graphic Design,</li><li><GoCheck />3D Artist</li></ul> <p className="aboutPar"> from a very young age, I've been curious about tech and art, and also I'm a self-taught person, that is what brought me here.
          <br />
          I really <span className="redBg">Love <b>GNU/Linux</b> <FaHeart className="love" /> systems</span> and free software as well digital sculpture and making 3D projects in general.
          <br />
          If you are interested in any particular idea, project, job, or just wanna hang out, do not hesitate to contact me through the form below, or through my <a href="https://t.me/gekkouart" target="_blank" rel="noreferrer">telegram<FaTelegramPlane /></a>.
        </p>
        <span onClick={handleClickLeer} className="aboutMore">
          Read more
          < IoIosArrowForward />
        </span>
      </div>
    </section >
  )
}
