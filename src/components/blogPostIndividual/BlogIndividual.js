import React from "react";
import "./blogIndividual.css";
import NavbarNew from 'components/navbarNew/NavbarNew';
import { MdDateRange } from 'react-icons/md';
import { IoIosArrowBack } from 'react-icons/io';
import { dataBlogPost } from "assets/blogposts/dataBlogPost";
import { useRoute } from "wouter";
import Footer from "components/footer/Footer";
import { Link } from 'wouter';
import ScrollTopButton from "assets/scrollTo/ScrollToButton";
import Comments from "components/commentSection/Comments";

export default function BlogIndividual() {

  const [match, params] = useRoute("/blog/:id");

  const post = dataBlogPost.find(e => {
    return e.title.replace(/\s+/g, '-').toLowerCase() === params.id;
  });

  const {
    id,
    title,
    fecha,
    category,
    text,
    comments
  } = post;

  const Parrafos = text.map((texto) => {
    return (
      <p key={texto} className="postPostDesc">
        {texto}
      </p>
    )
  });
  // console.log(dataBlogPost)

  return (
    <>
      <NavbarNew />
      <article className="postPost" key={id}>
        <div className="postPostTexts">
          <div className="postUpLink">
            <Link to="/blog">
              <IoIosArrowBack />
              Go Back
            </Link>
          </div>
          <h1 className="postPostTitle">
            {title}
          </h1>
          <span className="postPostFecha">
            <MdDateRange className="postSvgFecha" />
            Posted on a {fecha}
          </span>
          {Parrafos}
          <div className="contPostBtn">
            {category.map((tag) => {
              return (
                <button key={tag} className="postBtn">
                  {tag}
                </button>
              )
            })}
          </div>
        </div>
        {/* <Comments comments={comments} /> */}
        <ScrollTopButton />
      </article>
      <Footer />
    </>
  );
};
