import React, { useRef, useState } from "react";
import { useNav } from 'hooks/useNav';
import './contact.css';
import { Formik } from "formik";
import emailjs from 'emailjs-com';

export default function Contact() {
  const contactRef = useNav("Contact");
  const [formSent, setFormSent] = useState(false);
  const form = useRef();

  return (
    <section id="contactSection"
      ref={contactRef}
      className="contact side">
      <p className="h3Titles h3TitlesWhite">
        Stay in touch
      </p>
      <div className="formContainer">
        <Formik
          initialValues={{
            name: "",
            email: "",
            subject: "",
            message: ""
          }}
          validate={(valores) => {
            let errores = {};

            if (!valores.name) {
              errores.name = 'Please enter your full name'
            } else if (!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(valores.name)) {
              errores.name = "Your name only is allowed to have letters and spaces"
            }

            if (!valores.email) {
              errores.email = 'Please enter your email'
            } else if (!/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/.test(valores.email)) {
              errores.email = "Your email only is allowed to have letters, numbers, dots, hyphen and underscore"
            }

            return errores;
          }}
          onSubmit={(valores, { resetForm }) => {
            resetForm();
            setFormSent(true);
            setTimeout(() => setFormSent(false), 5000)

            //El envia del correo con emailjs.
            emailjs.sendForm('smtpmail', 'template_qb83wry', form.current, 'P3NKlQ5Mg3wx45TQw')
              .then((result) => {
                console.log(result.text);
              }, (error) => {
                console.log(error.text);
              });

          }}>
          {({ values, errors, touched, handleSubmit, handleChange, handleBlur }) => (
            <form className="formWrapper" ref={form} onSubmit={handleSubmit}>
              <input
                type="text"
                placeholder="Name"
                name="name"
                value={values.name}
                onChange={handleChange}
                onBlur={handleBlur}
                className="formInput" />
              {touched.name && errors.name && <div className="formError">{errors.name}</div>}
              <input
                type="email"
                placeholder="Email"
                name="email"
                value={values.email}
                onChange={handleChange}
                onBlur={handleBlur}
                className="formInput" />
              {touched.email && errors.email && <div className="formError">{errors.email}</div>}
              <input
                type="text"
                placeholder="Subject"
                name="subject"
                value={values.subject}
                onChange={handleChange}
                onBlur={handleBlur}
                className="formInput" />
              <textarea
                id=""
                name="message"
                placeholder="Message"
                value={values.message}
                onChange={handleChange}
                onBlur={handleBlur}
                className="formInput formTextarea">
              </textarea>
              {
                formSent
                  ?
                  <button className="formButton">
                    Sent
                  </button>
                  :
                  <button className="formButton">
                    Send
                  </button>
              }
              {
                formSent &&
                <p className="formSent">
                  Successfully Sent!
                </p>
              }
            </form>
          )}
        </Formik>
      </div>
    </section >
  )
}
