import React from "react";
import { useNav } from 'hooks/useNav';
import './work.css';
import { dataPortfolio } from 'assets/portfolio/dataPortfolio';
import { AiFillGithub, AiFillGitlab, AiFillHtml5 } from 'react-icons/ai';
import { FaFigma, FaGitAlt, FaGlobe, FaLinux, FaReact } from 'react-icons/fa';
import { DiCss3 } from 'react-icons/di';
import { SiJavascript, SiNodedotjs } from 'react-icons/si';

export default function Work() {
  const worksRef = useNav("Works");

  const skillset = ['HTML5', 'CSS3', 'Javascript', 'NodeJs', 'React', 'Figma', 'Git', 'GitLab', 'GitHub', 'Linux'];
  const skillIcons = [<AiFillHtml5 />, <DiCss3 />, <SiJavascript />, <SiNodedotjs />, <FaReact />, <FaFigma />, <FaGitAlt />, <AiFillGitlab />, <AiFillGithub />, <FaLinux />];

  const skillList = skillset.map((item, index) => {
    return (
      <li key={index}>
        {skillIcons[index]}
        <br />
        {item}
      </li>
    )

  });

  const showDataPortfolio = dataPortfolio.map((card) => {
    return (
      <article className="card" key={card.id}>
        <img src={card.img} alt="Kosan" className="cardImg" />
        <div className="cardTexts">
          <h1 className="cardTitle">
            {card.title}
          </h1>
          <p className="cardDesc">
            {card.description}
          </p>
          <div className="cardBtns">
            <button className="cardBtn">
              <a
                href={card.linkPage}
                target="_blank"
                className="cardLinks"
                rel="noreferrer">
                <FaGlobe />
                Visit
              </a>
            </button>
            <button className="cardBtn">
              <a
                href={card.linkSource}
                target="_blank"
                className="cardLinks"
                rel="noreferrer">
                <AiFillGitlab />
                Source
              </a>
            </button>
          </div>
        </div>
      </article>
    )
  });

  return (
    <section id="worksSection"
      ref={worksRef}
      className="work side">
      <p className="h3Titles">
        Recent Projects
      </p>

      <div className="worksContainer">
        {showDataPortfolio}
      </div>

      <p className="h3Titles">
        Skillset
      </p>
      <ul className="skillsetUl">
        {skillList}
      </ul>

    </section>
  )
}
