import React, { useState } from "react";
import './comments.css';

export default function Comments({ comments }) {

  const [dataForm, setDataForm] = useState({
    name: '',
    email: '',
    website: '',
    message: ''
  });

  const handleSubmit = (event) => {
    event.preventDefault();
    comments.push({
      userName: dataForm.name,
      fecha: dataForm.email,
      website: dataForm.website,
      comment: dataForm.message
    });
    // console.log(dataForm)
    // console.log(comments)

  }
  const handleChange = (event) => {
    // console.log(event.target.value)
    setDataForm({
      ...dataForm,
      [event.target.name]: event.target.value
    })
  }

  let fechaComment = new Date();
  let dd = String(fechaComment.getDate()).padStart(2, '0');
  let mm = String(fechaComment.getMonth() + 1).padStart(2, '0'); //January is 0!
  let yyyy = fechaComment.getFullYear();

  fechaComment = mm + '/' + dd + '/' + yyyy;

  const Comentarios = comments.map((usuario, index) => {
    return (
      <div key={index} className="comRead" >
        <div className="comUserInfo">
          <p className="comUserName">
            {usuario.userName} •
          </p>
          <p className="comFecha">
            {usuario.fecha}
          </p>
        </div>
        <div className="comUserInfoWeb">
          <a className="comWebsite" href="https://gekkou.xyz/" target="_blank" rel="noreferrer">
            {usuario.website}
          </a>
        </div>
        <p className="comUserComment">
          {usuario.comment}
        </p>
      </div>
    );
  })


  return (
    <div className="commentsContainer" >
      <h2 className="comTitle">
        Add a comment
      </h2>

      <form className="comFormWrapper" onSubmit={handleSubmit} >
        <input
          type="text"
          placeholder="Name"
          name="name"
          required
          // value={nombre}
          onChange={handleChange}
          className="comFormInput" />
        <input
          type="email"
          placeholder="Email"
          name="email"
          // value={email}
          onChange={handleChange}
          className="comFormInput" />
        <input
          type="text"
          placeholder="Website"
          name="website"
          // value={website}
          onChange={handleChange}
          className="comFormInput" />
        <textarea
          id=""
          name="message"
          placeholder="Message"
          // value={message}
          onChange={handleChange}
          className="comFormInput comTextarea">
        </textarea>
        <div className="comFormBtns">
          <button className="comtBtn">
            Preview Comment
          </button>
          <button className="comtBtn">
            Post Comment
          </button>
        </div>
      </form>

      <div className="comReadContainer" >
        {Comentarios}
      </div>

    </div>
  )
}
