import React from "react";
import 'components/navbar/navbar.css';
import { AiFillHome, AiOutlineMail } from "react-icons/ai";
import { HiCode } from "react-icons/hi";
import { BsPersonCircle } from "react-icons/bs";
import { FaCommentAlt } from "react-icons/fa";
import { Link } from "wouter";

// map usado en este array de li
// const zip = (first, second) => first.map((e, i) => [e, second[i]]);

export default function Navbar() {
  const navLinks = ['Home', 'Works', 'About', 'Contact', 'Blog'];
  const navIcons = [<AiFillHome />, <HiCode />, <BsPersonCircle />, <AiOutlineMail />, <FaCommentAlt />];

  const menuLinks = navLinks.map((item, index) => {

    if (item === 'Blog') {
      return (
        <li key={index}
          className="navItems">
          <Link to="/blog">
            <button
              className="navBtnsVerde">
              {navIcons[index]}
              <span className="menuName">
                {item}
              </span>
            </button>
          </Link>
        </li>
      )
    }
    return (
      <li key={index}
        className="navItems">
        <Link to="/">
          <button
            className="navBtnsVerde">
            {navIcons[index]}
            <span className="menuName">
              {item}
            </span>
          </button>
        </Link>
      </li>
    )
  })

  // console.log(menuLinks);

  return (
    <div className="navbar">
      <nav className="navbarNav">
        <ul className="navList">
          {menuLinks}
        </ul>
      </nav>
    </div>
  )
}
