import React /*, { useContext } */ from "react";
import './navbar.css';
import { AiFillHome, AiOutlineMail } from "react-icons/ai";
import { HiCode } from "react-icons/hi";
import { BsPersonCircle } from "react-icons/bs";
// import { FaCommentAlt } from "react-icons/fa";
import { Link } from "wouter";
// import { NavContext } from "context/NavContext";

// map usado en este array de li
// const zip = (first, second) => first.map((e, i) => [e, second[i]]);

export default function Navbar() {
  // const { activeLinkId } = useContext(NavContext)
  // console.log(activeLinkId)

  //  BLOG SECTION CLOSE TIL FINISH, IT WILL BE IN ANOTHER BRANCH

  const navLinks = ['Home', 'Works', 'About', 'Contact'/*, 'Blog'*/];
  const navIcons = [<AiFillHome />, <HiCode />, <BsPersonCircle />, <AiOutlineMail />/*, <FaCommentAlt />*/];
  const ariaLabel = ['Home', 'Works', 'About', 'Contact'/*, 'Blog'*/];

  const menuLinks = navLinks.map((item, index) => {
    const scrollToId = `${item.toLowerCase()}Section`;

    const handleClickNav = () => {
      if (scrollToId === 'blogSection') {
        return;
      }
      document.getElementById(scrollToId).scrollIntoView({
        behavior: "smooth"
      })
    }

    if (item === 'Blog') {
      return (
        <li key={index}
          className="navItems">
          <Link to="blog">
            <button
              aria-label={ariaLabel[index]}
              onClick={handleClickNav}
              className="navBtns">
              {navIcons[index]}
              <span className="menuName">
                {item}
              </span>
            </button>
          </Link>
        </li>
      )
    }
    return (
      <li key={index}
        className="navItems">
        <button
          onClick={handleClickNav}
          className="navBtns">
          {navIcons[index]}
          <span className="menuName">
            {item}
          </span>
        </button>
      </li>
    )
  })

  // console.log(menuLinks);

  return (
    <div className="navbar">
      <nav className="navbarNav">
        <ul className="navList">
          {menuLinks}
        </ul>
      </nav>
    </div>
  )
}
