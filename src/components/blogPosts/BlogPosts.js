import React from "react";
import { MdDateRange } from 'react-icons/md';
import 'pages/blog/blog.css';
import { dataBlogPost } from 'assets/blogposts/dataBlogPost';
import { Link } from "wouter";

export default function BlogPosts() {

  const showDataBlogPost = dataBlogPost.slice(0).reverse().map((card) => {
    let etiqueta = card.category;
    let title = card.title;
    title = title.replace(/\s+/g, '-').toLowerCase();

    return (
      <article className="cardPost" key={card.id}>
        <Link to={`/blog/${title}`}>
          <div className="cardPostTexts">
            <h2 className="cardPostTitle">
              {card.title}
            </h2>
            <span className="cardPostFecha">
              <MdDateRange className="cardSvgFecha" />
              {card.fecha}
            </span>
            <p className="cardPostDesc">
              {card.description}
            </p>
            <div className="cardPostBtn">
              {etiqueta.map((tag) => <button key={tag} className="postBtn">{tag}</button>)}
            </div>
          </div>
        </Link>
      </article>
    )
  });

  return (
    <div className="blogPost">
      {showDataBlogPost}
    </div>
  )
}

