import React from "react";
import { FaLinkedin, FaTelegramPlane } from 'react-icons/fa';
import { AiFillGitlab, AiOutlineMail } from "react-icons/ai";
import './footer.css';

export default function Footer() {
  return (
    <section
      className="footer side">
      <ul className="fooIcons">
        <li>
          <a
            href="https://gitlab.com/gekkou"
            target="_blank"
            className="cardLinks"
            rel="noreferrer">
            <AiFillGitlab />
          </a>
        </li>
        <li>
          <a
            href="https://t.me/gekkouart"
            target="_blank"
            className="cardLinks"
            rel="noreferrer">
            <FaTelegramPlane />
          </a>
        </li>
        <li>
          <a
            href="https://www.linkedin.com/in/kevin-rizo-749654134/"
            target="_blank"
            className="cardLinks"
            rel="noreferrer">
            <FaLinkedin />
          </a>
        </li>
        <li>
          <a
            href="mailto:kevin@kevinrizo.com"
            target="_blank"
            className="cardLinks"
            rel="noreferrer">
            <AiOutlineMail />
          </a>
        </li>
      </ul>
      <p className="fooPar">
        Copyright © 2022 <a href="https://gitlab.com/gekkou" target="_blank" rel="noreferrer">Kevin Rizo</a>
      </p>
    </section>
  )
}
