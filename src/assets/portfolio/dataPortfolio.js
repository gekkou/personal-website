import kosan from 'media/images/kosan.jpg';
import booking from 'media/images/booking.jpg';

export const dataPortfolio = [
  {
    id: 1,
    img: kosan,
    title: "Kō San",
    description: "A simple app that i made that categorize videos of the Kō San media.",
    linkPage: "https://www.kosan.kevinrizo.com/",
    linkSource: "https://gitlab.com/gekkou"
  },
  {
    id: 2,
    img: booking,
    title: "Booking Clone App",
    description: "Fullstack Booking Clone App made on the MERN stack, MongoDB, Express js, React js and Node js.",
    linkPage: "https://www.blog.kevinrizo.com/",
    linkSource: "https://gitlab.com/gekkou/bookin-clone-app"
  }
]
