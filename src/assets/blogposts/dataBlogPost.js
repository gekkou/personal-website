export const dataBlogPost = [
  {
    id: 1,
    title: "Post Lorem ipsum dolor sit amet",
    fecha: "15 Sep 2020",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi blandit ante vitae libero fringilla porta. Maecenas accumsan augue eros, ut porta erat pulvinar mattis.",
    category: [
      "javascript", "react"
    ],
    text: ["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu justo eget ipsum laoreet posuere quis eget dui. Sed et libero odio. Curabitur finibus est non nisl elementum auctor. Phasellus vel sollicitudin lectus, eget faucibus metus. Pellentesque eget porta velit. Phasellus vulputate felis non feugiat pellentesque.", "Vivamus porttitor velit id tellus ullamcorper, ut suscipit nulla semper. Maecenas ante velit, sollicitudin non elit vitae, dapibus aliquam arcu. Sed at nisi aliquam, ullamcorper purus quis, hendrerit dolor. Ut sit amet posuere nisi. Cras sit amet eleifend erat, eu placerat justo. Integer mattis id sem eu tempus. Maecenas sed faucibus orci. Duis sit amet blandit metus, et cursus diam.", "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Fusce accumsan mi diam, a vehicula odio viverra nec. Nam pharetra nibh quis mollis sagittis. Suspendisse quis efficitur risus, vitae porttitor odio. Sed finibus in elit ut vestibulum. Sed blandit quis lacus non vestibulum. Vestibulum pharetra nisl a lobortis dictum. Fusce tincidunt dui ac tellus rhoncus, vitae scelerisque est vehicula. Etiam sed consectetur lectus, id rhoncus tellus.", "Sed efficitur dignissim pretium. Sed in justo sit amet enim feugiat tempus. Fusce tempus nibh sed ultricies placerat. Pellentesque in diam finibus, commodo mi id, facilisis elit. Phasellus vulputate diam a congue lobortis. Sed feugiat sapien ut nisl tempor, non iaculis magna luctus. Suspendisse potenti.", "Cras consectetur aliquam efficitur. Curabitur porta diam eu diam tempus malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Aenean molestie in erat at semper. Ut eu fermentum velit. Fusce gravida orci at dapibus bibendum. Praesent nec dolor nec ante scelerisque malesuada. "],
    comments: [
      {
        userName: "",
        fecha: "",
        website: "",
        comment: ""
      }
    ],
  },
  {
    id: 2,
    title: "Post 2",
    fecha: "15 Sep 2020",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi blandit ante vitae libero fringilla porta. Maecenas accumsan augue eros, ut porta erat pulvinar mattis.",
    category: [
      "html", "css"
    ],
    text: ["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu justo eget ipsum laoreet posuere quis eget dui. Sed et libero odio. Curabitur finibus est non nisl elementum auctor. Phasellus vel sollicitudin lectus, eget faucibus metus. Pellentesque eget porta velit. Phasellus vulputate felis non feugiat pellentesque.", "Vivamus porttitor velit id tellus ullamcorper, ut suscipit nulla semper. Maecenas ante velit, sollicitudin non elit vitae, dapibus aliquam arcu. Sed at nisi aliquam, ullamcorper purus quis, hendrerit dolor. Ut sit amet posuere nisi. Cras sit amet eleifend erat, eu placerat justo. Integer mattis id sem eu tempus. Maecenas sed faucibus orci. Duis sit amet blandit metus, et cursus diam.", "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Fusce accumsan mi diam, a vehicula odio viverra nec. Nam pharetra nibh quis mollis sagittis. Suspendisse quis efficitur risus, vitae porttitor odio. Sed finibus in elit ut vestibulum. Sed blandit quis lacus non vestibulum. Vestibulum pharetra nisl a lobortis dictum. Fusce tincidunt dui ac tellus rhoncus, vitae scelerisque est vehicula. Etiam sed consectetur lectus, id rhoncus tellus.", "Sed efficitur dignissim pretium. Sed in justo sit amet enim feugiat tempus. Fusce tempus nibh sed ultricies placerat. Pellentesque in diam finibus, commodo mi id, facilisis elit. Phasellus vulputate diam a congue lobortis. Sed feugiat sapien ut nisl tempor, non iaculis magna luctus. Suspendisse potenti.", "Cras consectetur aliquam efficitur. Curabitur porta diam eu diam tempus malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Aenean molestie in erat at semper. Ut eu fermentum velit. Fusce gravida orci at dapibus bibendum. Praesent nec dolor nec ante scelerisque malesuada. "],
    comments: [
      {
        userName: "Lyxya",
        fecha: "08/15/2022",
        website: "https://Lyxya.gamez/",
        comment: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu justo eget ipsum laoreet posuere quis eget dui. Sed et libero odio. Curabitur finibus est non nisl elementum auctor. Phasellus vel sollicitudin lectus, eget faucibus metus. Pellentesque eget porta velit. Phasellus vulputate felis non feugiat pellentesque."
      },
      {
        userName: "gekkou",
        fecha: "08/13/2022",
        website: "https://gekkou.xyz/",
        comment: "Cras consectetur aliquam efficitur. Curabitur porta diam eu diam tempus malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Aenean molestie in erat at semper. Ut eu fermentum velit. Fusce gravida orci at dapibus bibendum. Praesent nec dolor nec ante scelerisque malesuada."
      }
    ],
  },
  {
    id: 3,
    title: "Post 3",
    fecha: "15 Sep 2020",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi blandit ante vitae libero fringilla porta. Maecenas accumsan augue eros, ut porta erat pulvinar mattis.",
    category: [
      "c++", "python"
    ],
    text: ["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu justo eget ipsum laoreet posuere quis eget dui. Sed et libero odio. Curabitur finibus est non nisl elementum auctor. Phasellus vel sollicitudin lectus, eget faucibus metus. Pellentesque eget porta velit. Phasellus vulputate felis non feugiat pellentesque.", "Vivamus porttitor velit id tellus ullamcorper, ut suscipit nulla semper. Maecenas ante velit, sollicitudin non elit vitae, dapibus aliquam arcu. Sed at nisi aliquam, ullamcorper purus quis, hendrerit dolor. Ut sit amet posuere nisi. Cras sit amet eleifend erat, eu placerat justo. Integer mattis id sem eu tempus. Maecenas sed faucibus orci. Duis sit amet blandit metus, et cursus diam.", "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Fusce accumsan mi diam, a vehicula odio viverra nec. Nam pharetra nibh quis mollis sagittis. Suspendisse quis efficitur risus, vitae porttitor odio. Sed finibus in elit ut vestibulum. Sed blandit quis lacus non vestibulum. Vestibulum pharetra nisl a lobortis dictum. Fusce tincidunt dui ac tellus rhoncus, vitae scelerisque est vehicula. Etiam sed consectetur lectus, id rhoncus tellus.", "Sed efficitur dignissim pretium. Sed in justo sit amet enim feugiat tempus. Fusce tempus nibh sed ultricies placerat. Pellentesque in diam finibus, commodo mi id, facilisis elit. Phasellus vulputate diam a congue lobortis. Sed feugiat sapien ut nisl tempor, non iaculis magna luctus. Suspendisse potenti.", "Cras consectetur aliquam efficitur. Curabitur porta diam eu diam tempus malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Aenean molestie in erat at semper. Ut eu fermentum velit. Fusce gravida orci at dapibus bibendum. Praesent nec dolor nec ante scelerisque malesuada. "],
    comments: [
      {
        userName: "",
        fecha: "",
        website: "",
        comment: ""
      }
    ],
  },
  {
    id: 4,
    title: "Post 4",
    fecha: "15 Sep 2020",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi blandit ante vitae libero fringilla porta. Maecenas accumsan augue eros, ut porta erat pulvinar mattis.",
    category: [
      "javascript", "node"
    ],
    text: ["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu justo eget ipsum laoreet posuere quis eget dui. Sed et libero odio. Curabitur finibus est non nisl elementum auctor. Phasellus vel sollicitudin lectus, eget faucibus metus. Pellentesque eget porta velit. Phasellus vulputate felis non feugiat pellentesque.", "Vivamus porttitor velit id tellus ullamcorper, ut suscipit nulla semper. Maecenas ante velit, sollicitudin non elit vitae, dapibus aliquam arcu. Sed at nisi aliquam, ullamcorper purus quis, hendrerit dolor. Ut sit amet posuere nisi. Cras sit amet eleifend erat, eu placerat justo. Integer mattis id sem eu tempus. Maecenas sed faucibus orci. Duis sit amet blandit metus, et cursus diam.", "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Fusce accumsan mi diam, a vehicula odio viverra nec. Nam pharetra nibh quis mollis sagittis. Suspendisse quis efficitur risus, vitae porttitor odio. Sed finibus in elit ut vestibulum. Sed blandit quis lacus non vestibulum. Vestibulum pharetra nisl a lobortis dictum. Fusce tincidunt dui ac tellus rhoncus, vitae scelerisque est vehicula. Etiam sed consectetur lectus, id rhoncus tellus.", "Sed efficitur dignissim pretium. Sed in justo sit amet enim feugiat tempus. Fusce tempus nibh sed ultricies placerat. Pellentesque in diam finibus, commodo mi id, facilisis elit. Phasellus vulputate diam a congue lobortis. Sed feugiat sapien ut nisl tempor, non iaculis magna luctus. Suspendisse potenti.", "Cras consectetur aliquam efficitur. Curabitur porta diam eu diam tempus malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Aenean molestie in erat at semper. Ut eu fermentum velit. Fusce gravida orci at dapibus bibendum. Praesent nec dolor nec ante scelerisque malesuada. "],
    comments: [
      {
        userName: "",
        fecha: "",
        website: "",
        comment: ""
      }
    ],
  },
  {
    id: 5,
    title: "Post 5",
    fecha: "15 Sep 2020",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi blandit ante vitae libero fringilla porta. Maecenas accumsan augue eros, ut porta erat pulvinar mattis.",
    category: [
      "javascript", "react"
    ],
    text: ["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu justo eget ipsum laoreet posuere quis eget dui. Sed et libero odio. Curabitur finibus est non nisl elementum auctor. Phasellus vel sollicitudin lectus, eget faucibus metus. Pellentesque eget porta velit. Phasellus vulputate felis non feugiat pellentesque.", "Vivamus porttitor velit id tellus ullamcorper, ut suscipit nulla semper. Maecenas ante velit, sollicitudin non elit vitae, dapibus aliquam arcu. Sed at nisi aliquam, ullamcorper purus quis, hendrerit dolor. Ut sit amet posuere nisi. Cras sit amet eleifend erat, eu placerat justo. Integer mattis id sem eu tempus. Maecenas sed faucibus orci. Duis sit amet blandit metus, et cursus diam.", "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Fusce accumsan mi diam, a vehicula odio viverra nec. Nam pharetra nibh quis mollis sagittis. Suspendisse quis efficitur risus, vitae porttitor odio. Sed finibus in elit ut vestibulum. Sed blandit quis lacus non vestibulum. Vestibulum pharetra nisl a lobortis dictum. Fusce tincidunt dui ac tellus rhoncus, vitae scelerisque est vehicula. Etiam sed consectetur lectus, id rhoncus tellus.", "Sed efficitur dignissim pretium. Sed in justo sit amet enim feugiat tempus. Fusce tempus nibh sed ultricies placerat. Pellentesque in diam finibus, commodo mi id, facilisis elit. Phasellus vulputate diam a congue lobortis. Sed feugiat sapien ut nisl tempor, non iaculis magna luctus. Suspendisse potenti.", "Cras consectetur aliquam efficitur. Curabitur porta diam eu diam tempus malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Aenean molestie in erat at semper. Ut eu fermentum velit. Fusce gravida orci at dapibus bibendum. Praesent nec dolor nec ante scelerisque malesuada. "],
    comments: [
      {
        userName: "",
        fecha: "",
        website: "",
        comment: ""
      }
    ],
  },
  {
    id: 6,
    title: "Post 6",
    fecha: "15 Sep 2020",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi blandit ante vitae libero fringilla porta. Maecenas accumsan augue eros, ut porta erat pulvinar mattis.",
    category: [
      "javascript", "react"
    ],
    text: ["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu justo eget ipsum laoreet posuere quis eget dui. Sed et libero odio. Curabitur finibus est non nisl elementum auctor. Phasellus vel sollicitudin lectus, eget faucibus metus. Pellentesque eget porta velit. Phasellus vulputate felis non feugiat pellentesque.", "Vivamus porttitor velit id tellus ullamcorper, ut suscipit nulla semper. Maecenas ante velit, sollicitudin non elit vitae, dapibus aliquam arcu. Sed at nisi aliquam, ullamcorper purus quis, hendrerit dolor. Ut sit amet posuere nisi. Cras sit amet eleifend erat, eu placerat justo. Integer mattis id sem eu tempus. Maecenas sed faucibus orci. Duis sit amet blandit metus, et cursus diam.", "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Fusce accumsan mi diam, a vehicula odio viverra nec. Nam pharetra nibh quis mollis sagittis. Suspendisse quis efficitur risus, vitae porttitor odio. Sed finibus in elit ut vestibulum. Sed blandit quis lacus non vestibulum. Vestibulum pharetra nisl a lobortis dictum. Fusce tincidunt dui ac tellus rhoncus, vitae scelerisque est vehicula. Etiam sed consectetur lectus, id rhoncus tellus.", "Sed efficitur dignissim pretium. Sed in justo sit amet enim feugiat tempus. Fusce tempus nibh sed ultricies placerat. Pellentesque in diam finibus, commodo mi id, facilisis elit. Phasellus vulputate diam a congue lobortis. Sed feugiat sapien ut nisl tempor, non iaculis magna luctus. Suspendisse potenti.", "Cras consectetur aliquam efficitur. Curabitur porta diam eu diam tempus malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Aenean molestie in erat at semper. Ut eu fermentum velit. Fusce gravida orci at dapibus bibendum. Praesent nec dolor nec ante scelerisque malesuada. "],
    comments: [
      {
        userName: "",
        fecha: "",
        website: "",
        comment: ""
      }
    ]
  },
  {
    id: 7,
    title: "Post 7",
    fecha: "15 Sep 2020",
    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi blandit ante vitae libero fringilla porta. Maecenas accumsan augue eros, ut porta erat pulvinar mattis.",
    category: [
      "javascript", "react"
    ],
    text: ["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu justo eget ipsum laoreet posuere quis eget dui. Sed et libero odio. Curabitur finibus est non nisl elementum auctor. Phasellus vel sollicitudin lectus, eget faucibus metus. Pellentesque eget porta velit. Phasellus vulputate felis non feugiat pellentesque.", "Vivamus porttitor velit id tellus ullamcorper, ut suscipit nulla semper. Maecenas ante velit, sollicitudin non elit vitae, dapibus aliquam arcu. Sed at nisi aliquam, ullamcorper purus quis, hendrerit dolor. Ut sit amet posuere nisi. Cras sit amet eleifend erat, eu placerat justo. Integer mattis id sem eu tempus. Maecenas sed faucibus orci. Duis sit amet blandit metus, et cursus diam.", "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Fusce accumsan mi diam, a vehicula odio viverra nec. Nam pharetra nibh quis mollis sagittis. Suspendisse quis efficitur risus, vitae porttitor odio. Sed finibus in elit ut vestibulum. Sed blandit quis lacus non vestibulum. Vestibulum pharetra nisl a lobortis dictum. Fusce tincidunt dui ac tellus rhoncus, vitae scelerisque est vehicula. Etiam sed consectetur lectus, id rhoncus tellus.", "Sed efficitur dignissim pretium. Sed in justo sit amet enim feugiat tempus. Fusce tempus nibh sed ultricies placerat. Pellentesque in diam finibus, commodo mi id, facilisis elit. Phasellus vulputate diam a congue lobortis. Sed feugiat sapien ut nisl tempor, non iaculis magna luctus. Suspendisse potenti.", "Cras consectetur aliquam efficitur. Curabitur porta diam eu diam tempus malesuada. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Aenean molestie in erat at semper. Ut eu fermentum velit. Fusce gravida orci at dapibus bibendum. Praesent nec dolor nec ante scelerisque malesuada. "],
    comments: [
      {
        userName: "",
        fecha: "",
        website: "",
        comment: ""
      }
    ],
  },
]
