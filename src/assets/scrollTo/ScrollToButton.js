import React from "react";
import { BsFillArrowUpSquareFill } from 'react-icons/bs';
import './scrollTo.css';

const ScrollTopButton = () => {

  const scrollTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    });
  };
  return (
    <div className="scrollTopButton">
      <BsFillArrowUpSquareFill
        onClick={scrollTop}
      />
    </div>
  );
};

export default ScrollTopButton;
