import { useEffect } from "react";
import { useLocation } from "wouter";

const ScrollTop = (props) => {
  const location = useLocation();
  useEffect(() => {
    window.scrollTo(0, 0);
  }, [location]);

  return <>{props.children}</>
};

export default ScrollTop;
