import React from 'react';
import 'styles/main.css'
import Home from 'pages/home/Home';
import Blog from 'pages/blog/Blog';
import ScrollTop from 'assets/scrollTo/ScrollTop';
import BlogIndividual from 'components/blogPostIndividual/BlogIndividual';
import { Redirect, Route, Switch } from 'wouter';

function App() {
  return (
    <div className="App">
      <ScrollTop>
        <Switch>
          <Route component={Home} path='/' />
          <Route component={Blog} path='/blog' />
          <Route component={BlogIndividual} path='/blog/:id' />
          <Redirect to='/' />
        </Switch>
      </ScrollTop>
    </div>
  );
}

export default App;
